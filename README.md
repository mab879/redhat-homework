# RedHat Interview Homework
## Problem 1
### Prompt
Write a Bash script or an Ansible Playbook that configures the system according to these three requirements:
* users in a Linux system can set only passwords that are n characters long
* umask is set to an appropriate value
* user generation of core dumps is disabled

## Problem 2 
### Prompt
2. Write an Ansible playbook that would make sure that a file has correct contents. All of the following conditions must be satisfied:
* Contents of the file would be supplied as an Ansible variable.
* File contents have to match that variable exactly - there is nothing extra in the file (not even whitespace).
* If the file content is correct before the playbook execution, the playbook won’t modify it during its execution.